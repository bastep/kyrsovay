#pragma once
#pragma once
#include"Constants.h"
struct slink
{
	slink* next;
	slink()
	{
		next = 0;
	}
	slink(slink* p)
	{
		next = p;
	}
};

class slist_base {
	slink* last;
public:
	int kol = 0;
	slink* head()
	{
		return last ? last->next : 0;
	}
	void pop_back();
	void append(slink*);
	void insert(slink*);
	void pop(int pos);
	void pop_front();
	int size() { return kol; }
	void clear() { last = 0; }
	slist_base() { last = 0; }
	slist_base(slink* a) { last = a->next = a; }
	friend class base_iterator;
};

class base_iterator
{
public:
	slink* ce;
	slist_base* cs;
	base_iterator(slist_base& s);
	slink* operator()();
	slink* nxt();
	void pop(int pos);
	void clear();
	slink* poisk(int pos);
};

template <class t> class iterator;

template<class t>
class list : public slist_base {
public:
	void append(t* a)
	{
		slist_base::append(a);
	}
	void insert(t* a)
	{
		slist_base::insert(a);
	}

	t* get()
	{
		return (t*)slist_base::get();
	}
	int size()
	{
		return slist_base::size();
	}
	void pop_front()
	{
		slist_base::pop_front();
	}
	friend class iterator<t>;
};

template <class t>
class iterator : public base_iterator
{
public:
	iterator(list<t>& s) : base_iterator(s) {};
	t* nxt()
	{
		return (t*)(base_iterator::nxt());
	}

	t* poisk(int pos)
	{
		return (t*)base_iterator::poisk(pos);
	}
	t* operator()()
	{
		return (t*)(base_iterator::operator()());
	}
};

class os
	:public slink
{
protected:
	char Name[100];
	int Year;
	bool OpenCode;

public:
	virtual void vvod();
	virtual void vivod();
	os();
	os(const char[100],int, bool);
	~os();
	friend std::ostream& operator << (std::ostream& stream, os& point);
};